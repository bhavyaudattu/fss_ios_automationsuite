package steps.stepDefinitions;

import org.testng.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import testBase.pagesCommon.WelcomePage;
import testBase.testCommon.Copy;
import testBase.testCommon.Pages;

public class WelcomePageSteps {
	
	Pages pageObjects;
	WelcomePage welcomePageObj;
	
	public WelcomePageSteps(Pages pages) {
		pageObjects = pages;
		welcomePageObj = pageObjects.getWelcompageObject();
	}
	
	@When("^I launch the FSS Application$")
	public void i_launch_the_FSS_Application() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		// launch the APP 
		welcomePageObj.launchApp();
	}

	@When("^I navigate to the Login Screen$")
	public void i_navigate_to_the_Login_Screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
//		Thread.sleep(3000);
//		welcomePageObj.swipeWelcomeInfo();
//		Thread.sleep(3000);
//		welcomePageObj.swipeWelcomeInfo();
//		Thread.sleep(3000);
//		welcomePageObj.tapDoneBtn();
		Thread.sleep(3000);
		welcomePageObj.tapSkip();
		Thread.sleep(3000);
	}

	@Then("^show splash screen as normal$")
	public void show_splash_screen_as_normal() throws Throwable {
		
	    // Check the splash screen
		 Assert.assertNotNull(welcomePageObj.checkSplashScreen(), "Welcomescreen label one is NOT as expected");
	}

	@Then("^then show the first welcome screen$")
	public void then_show_the_first_welcome_screen() throws Throwable {
	   
		//verify the welcome screen elements
		Assert.assertEquals(welcomePageObj.getWelcomeScreenMessage01(), Copy.WELCOME_SCREEN_LABEL_ONE, 
	    		"Welcomescreen label one is NOT as expected");
		Assert.assertEquals(welcomePageObj.getWelcomeScreenSubMessage01() , Copy.WELCOME_SCREEN_SUB_MESSAGE_ONE, 
	    		"Welcomescreen label one is NOT as expected");
		Assert.assertNotNull(welcomePageObj.checkWelcomePageIndicator(), "Welcomescrren page indicator is NOT shown");
		Assert.assertNotNull(welcomePageObj.checkSkipBtn(), "Welcomescreen SKIP button is NOT shown");
		Assert.assertEquals(welcomePageObj.getSkipBtnText() , Copy.WELCOME_SCREEN_SKIP_BTN, 
	    		"Welcomescreen Skip button text NOT as expected");
		Assert.assertNotNull(welcomePageObj.checkNextBtn(), "Welcomescreen Next button is NOT shown");
		Assert.assertEquals(welcomePageObj.getNextBtnText() , Copy.WELCOME_SCREEN_NEXT_BTN, 
	    		"Welcomescreen Next button text NOT as expected");
		welcomePageObj.swipeWelcomeInfo();
		Assert.assertEquals(welcomePageObj.pageNumber(), "page 2 of 3","welcome page swipe fails");
		Assert.assertEquals(welcomePageObj.getWelcomeScreenMessage02(), Copy.WELCOME_SCREEN_LABEL_TWO, 
	    		"Welcomescreen label one is NOT as expected");
		Assert.assertEquals(welcomePageObj.getWelcomeScreenSubMessage02() , Copy.WELCOME_SCREEN_SUB_MESSAGE_TWO, 
	    		"Welcomescreen label one is NOT as expected");
		welcomePageObj.swipeWelcomeInfo();
		Assert.assertEquals(welcomePageObj.pageNumber(), "page 3 of 3","welcome page swipe fails");
		Assert.assertEquals(welcomePageObj.getWelcomeScreenMessage03(), Copy.WELCOME_SCREEN_LABEL_THREE, 
	    		"Welcomescreen label one is NOT as expected");
		Assert.assertEquals(welcomePageObj.getWelcomeScreenSubMessage03() , Copy.WELCOME_SCREEN_SUB_MESSAGE_THREE, 
	    		"Welcomescreen label one is NOT as expected");
		Assert.assertNotNull(welcomePageObj.checkDoneBtn(), "Welcomescreen Next button is NOT shown");
		welcomePageObj.tapDoneBtn();
		
	}
	
}
