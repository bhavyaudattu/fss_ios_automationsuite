package steps.stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import testBase.pagesCommon.ConsolidateWebViewPage;
import testBase.pagesCommon.DashboardPage;
import testBase.testCommon.Pages;

import java.util.List;
import java.util.Map;

public class DashboardSteps {
    Pages pageObjects;
    DashboardPage dashboardPage;
    ConsolidateWebViewPage consolidateWebViewPage;

    public DashboardSteps(Pages pages) {
        pageObjects = pages;
        dashboardPage = pageObjects.getDasboardPageObj();
    }

    @Then("^I verify I am on the Dashboard Page$")
    public void navigateToProfileScreen() {
        Assert.assertNotNull(dashboardPage.checkForTheGraph(), "The Graph is not present");
    }

    @Then("^I verify the following details on the Dashboard$")
    public void verifyTheDetailsOnTheDashboard(DataTable dataTable) throws InterruptedException {
        Thread.sleep(5000);
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        dashboardPage.verifyTheDashboardPageDetails(list);
    }

    @Then("^I verify the details of Consolidate My Super card$")
    public void verifyTheContentsOfConsolidateMySuper() {

        Assert.assertNotNull(dashboardPage.checkConsolidateButton(), "The Consolidate Button is not present");
        Assert.assertNotNull(dashboardPage.checkConsolidateDescription(), "The Consolidate Description is not present");
        Assert.assertNotNull(dashboardPage.checkConsolidateTitle(), "The Consolidate Title is not present");
    }

    @When("^I click on the Consolidate Now button$")
    public void clickOnConsolidateNowButton() throws InterruptedException {
        dashboardPage.clickOnConsolidateNowButton();
    }

    @Then("^I verify consolidate web view page details$")
    public void verifyConsolidateWebViewPage() throws InterruptedException {
        Thread.sleep(5000);
        Assert.assertNotNull(consolidateWebViewPage.checkConsolidateTitle(), "The Consolidate Title is not present");
        Assert.assertNotNull(consolidateWebViewPage.checkDoneButton(), "The Done button is not present");
        Assert.assertNotNull(consolidateWebViewPage.checkPersonalDetailsHeading(), "The Personal Details Heading is not present");
        Assert.assertNotNull(consolidateWebViewPage.checkReloadButton(), "The Reload Button is not present");
    }
}