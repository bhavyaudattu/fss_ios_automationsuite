package steps.stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import testBase.pagesCommon.LoginPage;
import testBase.testCommon.Pages;

import java.util.List;

public class LoginPageSteps {
    Pages pageObjects;
    LoginPage loginPage;

    public LoginPageSteps(Pages pages) {
        pageObjects = pages;
        loginPage = pageObjects.getLoginPageObject();
    }

    @Given("^I am not currently logged in$")
    public void i_am_not_currently_logged_in() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertNotNull(loginPage.checkLoginBtn(), "Login page not shown!");
    }

    @When("^the splash screens finish showing$")
    public void the_splash_screens_finish_showing() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertNotNull(loginPage.checkLoginInputFieldEmail(), "Login page -Input filed memberNumber/email not shown!");
    }

    @Then("^show the login screen with ghost text$")
    public void show_the_login_screen_with_ghost_text() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.clearLoginField();
        //Assert.assertEquals(loginPage.getLoginInputFieldEmailDefaultText(), "Email/Member Number");
        Assert.assertEquals(loginPage.getLoginInputFieldPwdDefaultText(), "Password");
    }

    @Given("^I am on the login screen$")
    public void i_am_on_the_login_screen() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertNotNull(loginPage.checkLoginBtn(), "Login page not shown!");
        // Assert.assertNotNull(loginPage.checkLoginInputFieldEmail(), "Login page -Input filed memberNumber/email not shown!");
        // Assert.assertEquals(loginPage.getLoginInputFieldEmailDefaultText() , "Email/Member Number");
        // Assert.assertEquals(loginPage.getLoginInputFieldPwdDefaultText(), "Password");
    }

    @When("^I select the email/member number field$")
    public void i_select_the_email_member_number_field() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.tapEmailMemberId();
    }

    @Then("^show cursor in the field$")
    public void show_cursor_in_the_field() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^open keyboard with CTA of 'next' which will shift the focus to the next field password$")
    public void open_keyboard_with_CTA_of_next_which_will_shift_the_focus_to_the_next_field_password() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //Assert.assertTrue(loginPage.isKeyboardShownInLoginPage(), "Keyboard not shown in Login Page!");
        //loginPage.tapEmailMemberId();
        loginPage.tapNextInKeyBoard();
    }

    @Then("^hide ghost text when the first character is entered$")
    public void hide_ghost_text_when_the_first_character_is_entered() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.clearLoginField();
        loginPage.enterEmailMemberId("abc");
        //Assert.assertEquals(loginPage.getLoginInputFieldEmailDefaultText(), "abc", "Entered text don't match!");

    }

    @When("^I select the password field$")
    public void i_select_the_password_field() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.tapPwd();
        Assert.assertTrue(loginPage.keyBoardReturnElementPresent(), "Passowrd field not selected");
    }

    @Then("^open keyboard with CTA of 'return' which will dismiss the keyboard$")
    public void open_keyboard_with_CTA_of_return_which_will_dismiss_the_keyboard() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.tapReturnInKeyBoard();
        Thread.sleep(2000);
        Assert.assertFalse(loginPage.keyBoardReturnElementPresent(), "Keyboard is not dismissed!");

    }

    @Then("^mask each character entered in the password field$")
    public void mask_each_character_entered_in_the_password_field() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.enterPwd("Pillar/1");
        Assert.assertEquals(loginPage.getPwdFieldText(), "••••••••", "Pwd is not masked");
    }

    @Given("^I have not entered all fields$")
    public void i_have_not_entered_all_fields() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals(loginPage.getLoginInputFieldEmailDefaultText(), "Email");
        Assert.assertEquals(loginPage.getLoginInputFieldPwdDefaultText(), "Password");
    }

    @When("^I select the login button$")
    public void i_select_the_login_button() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.tapLoginBtn();
        Thread.sleep(15000);
        // Assert.assertEquals(loginPage.emptyEmailFiledErrorValue(), "Please enter your email/member number.");
        // Assert.assertEquals(loginPage.emptyPwdFiledErrorValue(), "Please enter your password.");

    }

    @Then("^Assert for Incorrect Information popup$")
    public void check_for_incorrect_information_popup() throws Throwable {
        Assert.assertNotNull(loginPage.checkIncorrectInfoPopupTitle(), "Incorrect Info Popup Title Not Found");
        Assert.assertNotNull(loginPage.checkIncorrectInfoPopupMessage(), "Incorrect Info Popup Message Not Found");
    }

    @Then("^I enter \"([^\"]*)\" in the email/member Id field$")
    public void i_enter(String arg1, DataTable arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        List<List<String>> data = arg2.raw();
        loginPage.clearLoginField();
        loginPage.enterEmail(data.get(0).get(0));
    }

    @Then("^I enter \"([^\"]*)\" in the password field$")
    public void i_enter_password(String arg1, DataTable arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        List<List<String>> data = arg2.raw();
        loginPage.enterPassword(data.get(0).get(0));
    }

    @Then("^I verify and close the face Id or touch Id popup$")
    public void verifyFaceIdPopup() throws Throwable {
        Thread.sleep(5000);
        String abc = "FOUND";
        if (loginPage.checkIfLoginTouchIdTitlePresent() != null) {
            Assert.assertTrue(true, "Touch Id is not found");
        } else if (loginPage.checkIfLoginFaceIdTitlePresent() != (null)) {
            Assert.assertTrue(true, "Face Id is not found");
        } else {
            Assert.fail("Both Touch Id and Face Id are not found");
        }
        loginPage.clickOnNotNow();
    }

    @Then("^I see inline errors for email/member and password fields$")
    public void verifyInlineErrorForBlankEmailAndPassword(DataTable arg2) throws Throwable {
        List<List<String>> data = arg2.raw();
        Thread.sleep(3000);
        Assert.assertEquals(loginPage.getEmailMemberNumberInlineErrorMessage(), data.get(0).get(0), "Inline error for email address doesn't match");
        Assert.assertEquals(loginPage.getPasswordInlineErrorMessage(), data.get(1).get(0), "Inline error for password field doesn't match");
    }

    @Then("^I verify the email/member number field value$")
    public void getEmailMemberNumberFieldValue(DataTable arg1) throws Throwable {
        List<List<String>> data = arg1.raw();
        Assert.assertEquals(loginPage.getEmailMemberNumberFieldValue(), data.get(0).get(0), "Masked email/member number fields do not match");
    }

    @Then("^I see NO inline errors for email/member and password fields$")
    public void verifyNoInlineErrorForBlankEmailAndPassword() throws Throwable {
        Thread.sleep(2000);
        Assert.assertNull(loginPage.getEmailMemberNumberInlineErrorMessage());
        Assert.assertNull(loginPage.getPasswordInlineErrorMessage());
    }

    @Then("^I navigate to the Profile Screen$")
    public void navigateToProfileScreen() throws InterruptedException {
        loginPage.navigateToProfilePage();

    }

    @Then("^I click on the Forgot Password Link$")
    public void clickOnForgotPasswordLink() throws Exception {
        loginPage.clickOnForgotPasswordLink();
    }

    @Then("^I verify contents of the Reset Password Web View$")
    public void validateResetPasswordWebView() throws Exception {
        Thread.sleep(4000);
        Assert.assertNotNull(loginPage.checkForResetPasswordTitle(), "Reset Password Text is not visible");
    }

    @Then("^I click on the Terms and Conditions Link$")
    public void clickOnTermsAndConditionsLink() {
        loginPage.clickOnTermsAndConditionsLink();
    }

    @Then("^I verify contents of the Terms and Conditions Web View$")
    public void validateTermsAndConditionsWebView() throws Exception {
        Thread.sleep(4000);
        Assert.assertNotNull(loginPage.checkForContentsOfTermsAndConditions(), "Terms and Conditions Text is not visible");
        Assert.assertNotNull(loginPage.checkForDoneButton(), "Done button is not present");
        Assert.assertNotNull(loginPage.checkForReloadButton(), "Reload button is not present");
    }

    @Then("^I click on the Done button$")
    public void clickOnDoneAndVerifyLoginPageContents() {
        loginPage.clickOnDoneButton();
    }

    @Then("^I verify I am on Login Page$")
    public void verifyLoginPage() {
        Assert.assertNotNull(loginPage.checkForLoginButton());
    }
}

