package steps.stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;
import testBase.pagesCommon.ProfilePage;
import testBase.testCommon.Copy;
import testBase.testCommon.Pages;

import java.util.List;
import java.util.Map;

public class ProfilePageSteps {
    Pages pageObjects;
    ProfilePage profilePage;

    public ProfilePageSteps(Pages pages) {
        pageObjects = pages;
        profilePage = pageObjects.getProfilePageObj();
    }

    @Then("^I verify the card details on the Profile Screen$")
    public void validateTheProfilePageCardDetails(DataTable dataTable) throws Throwable {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        profilePage.verifyMembershipDetailsCard(list);
    }

    @Then("^I verify for the following fields on the Profile Section$")
    public void validateTheProfileSectionFieldsAndValues(DataTable dataTable) throws Throwable {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        System.out.println("Verifying the other details of the profile section");
        profilePage.verifyProfilePageDetails(list);
    }

    @Then("^I click on Share Membership Details$")
    public void clickOnShareMembershipDetails() throws InterruptedException {
        profilePage.clickOnShareMembershipDetails();

    }

    @Then("^I verify the contents of the Email Subject$")
    public void verifyTheShareMembershipEmailContents() throws InterruptedException {
        Assert.assertEquals(profilePage.getShareMemEmailSubject(), Copy.EMAIL_SUBJECT,
                "Share Membership Email Subject does not match");
    }

    @Then("^I verify the contents of the Email Body")
    public void verifyTheShareMembershipEmailBody(DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        System.out.println("Verifying the other details of the profile section");
        profilePage.verifyProfilePageDetails(list);
    }

    @Given("^I scroll down to view the other contents$")
    public void scrollDownToViewTheContents() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        profilePage.scrollDownToSecondHalf();
    }
}
