package testBase.pagesCommon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ConsolidateWebViewPage extends BasePage {
    public ConsolidateWebViewPage(AppiumDriver driver) {
        super(driver);
    }

    private By consolidateTitle = MobileBy.xpath("//XCUIElementTypeStaticText[@name=\"Consolidate your super\"]");
    private By personalDetailsHeading = MobileBy.xpath("//XCUIElementTypeStaticText[@name=\"1. Personal details\"]");
    private By reloadButton = MobileBy.xpath("//XCUIElementTypeButton[@name=\"ReloadButton\"]");
    private By doneButton = MobileBy.xpath("//XCUIElementTypeButton[@name=\"Done\"]");

    public WebElement checkConsolidateTitle() {

        return find(consolidateTitle, 60);
    }

    public WebElement checkPersonalDetailsHeading() {

        return find(personalDetailsHeading);
    }

    public WebElement checkReloadButton() {

        return find(reloadButton);
    }

    public WebElement checkDoneButton() {

        return find(doneButton);
    }


}
