package testBase.pagesCommon;

import com.google.common.base.Function;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.FluentWait;
import testBase.testCommon.TestBase;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("rawtypes")
public class BasePage extends TestBase {

    private By spaceKey = MobileBy.AccessibilityId("space");
    private By nextKey = MobileBy.AccessibilityId("Next:");

    public BasePage(AppiumDriver driver) {
        this.driver = driver;
    }

    /**
     * This method return the list of element, if not found fluentwaits for timeperiod passed or default
     * When not found returns null
     *
     * @param locator type
     * @author Basav
     */
    protected WebElement find(final By locator, int... args) {

        int timeout = (args.length > 0 ? args[0] : 15);
        try {
            FluentWait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
                    .pollingEvery(Duration.ofMillis(2000)).ignoring(Exception.class)
                    .ignoring(NoSuchElementException.class);

            WebElement webelement = wait.until(new Function<AppiumDriver, WebElement>() {
                @Override
                public WebElement apply(AppiumDriver appiumDriver) {

                    return appiumDriver.findElement(locator);
                }
            });
            return webelement;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This method return the list of elements, if not found fluentwaits for timeperiod passed or default
     * When not found returns null
     *
     * @param locator type
     * @author Basav
     */
    protected List<WebElement> finds(final By locator, int... args) {

        int timeout = (args.length > 0 ? args[0] : 15);
        List<WebElement> webelements = null;

        try {

            FluentWait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
                    .pollingEvery(Duration.ofMillis(200)).ignoring(Exception.class)
                    .ignoring(NoSuchElementException.class);

            webelements = wait.until(new Function<AppiumDriver, List<WebElement>>() {
                @Override
                @SuppressWarnings("unchecked")
                public List<WebElement> apply(AppiumDriver appiumDriver) {
                    return appiumDriver.findElements(locator);
                }
            });
        } catch (Exception e) {
            return null;
        }
        return webelements;
    }

    /**
     * This method tap passed the element
     *
     * @param locator type
     * @author Basav
     */
    protected void tapElement(By locator) {
        WebElement element = find(locator, 30);
        element.click();
    }

    /**
     * This method tap passed the element
     *
     * @param locator type
     * @author Basav
     */
    protected void tapElement(By locator, int timeout) {
        WebElement element = find(locator, timeout);
        element.click();
    }

    /**
     * This method check if the element is enabled
     *
     * @param locator type
     * @author Basav
     */
    protected boolean isEnabled(By locator) {
        String text = find(locator).getAttribute("enabled");
        return Boolean.parseBoolean(text);
    }

    /**
     * This method check if the element is visible
     *
     * @param locator type
     * @author Bhavya Udattu
     */
    protected boolean isVisible(By locator) {
        String text = find(locator).getAttribute("visible");
        return Boolean.parseBoolean(text);
    }


    /**
     * This method check if the element with static text is visible
     *
     * @param staticText
     * @author Bhavya Udattu
     */
    protected boolean isElementWithStaticTextVisible(String staticText) {
        String xpathForStaticText = "//XCUIElementTypeStaticText[@name=\"" + staticText + "\"]";
        System.out.println("This is the xpath " + xpathForStaticText);
        By elementRequired = MobileBy.xpath(xpathForStaticText);
        String text = find(elementRequired, 60).getAttribute("visible");
        System.out.println("This is attribute value" + find(elementRequired, 60).getAttribute("name"));
        return Boolean.parseBoolean(text);
    }

    /**
     * This method check if the element with static text is enabled
     *
     * @param staticText
     * @author Bhavya Udattu
     */
    protected boolean isElementWithStaticTextEnabled(String staticText) {
        String xpathForStaticText = "//XCUIElementTypeStaticText[@name=\"" + staticText + "\"]";
        System.out.println("This is the xpath " + xpathForStaticText);
        By elementRequired = MobileBy.xpath(xpathForStaticText);
        String text = find(elementRequired, 60).getAttribute("enabled");
        System.out.println("This is attribute value" + find(elementRequired, 60).getAttribute("name"));
        return Boolean.parseBoolean(text);
    }

    /**
     * This method get the element text
     *
     * @param locator type
     * @author Basav
     */
    protected String getText(By locator) {
        String text = find(locator).getText();
        return text;
    }

    /**
     * This method get the element name
     *
     * @param locator type
     * @author Basav
     */
    protected String getName(By locator) {
        String text = find(locator).getAttribute("name");
        return text;
    }

    /**
     * This method scrolls based upon the passed parameters
     *
     * @param locator type
     * @author Basav
     */
    @Override
    public void scrollToWebElement(By locator) {

        RemoteWebElement element = (RemoteWebElement) find(locator);
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "any value");
        driver.executeScript("mobile:scroll", scrollObject);
    }

    /**
     * This method scrolls based upon the passed parameters
     *
     * @author Basav
     * // @param int startx - the starting x position
     * // @param int starty - the starting y position
     * // @param int endx - the ending x position
     * //@param int endy - the ending y position
     */
    public void scroll(By locator, int startx, int starty, int endx, int endy) {

        TouchAction touchAction = new TouchAction(driver);

        int numOfSwipes = 0;
        while (find(locator, 7) == null && numOfSwipes <= 6) {
            touchAction.longPress(PointOption.point(startx, starty))
                    .moveTo(PointOption.point(endx, endy))
                    .release()
                    .perform();
            numOfSwipes++;
        }
    }

    /**
     * Tap an element given the coordinates x and y
     *
     * @param x integer
     * @param y integer
     * @author Bhavya Udattu
     */
    public void tapByCoordinates(int x, int y) {
        TouchAction touchAction = new TouchAction(driver);
        touchAction.press(PointOption.point(x, y)).release();
        touchAction.perform();
        System.out.println("Tapped by cordinates");
    }

    /**
     * Tap element by coordinates given the element locator
     *
     * @param locator locator
     * @author Bhavya Udattu
     */
    public void tapByCoordinates(By locator) throws InterruptedException {
        Point location = driver.findElement(locator).getLocation();
        TouchAction touchAction = new TouchAction(driver);
        touchAction.press(PointOption.point(location));
        touchAction.perform();
        System.out.println("Tapping ");
    }

    protected void scroll(int startx, int starty, int endx, int endy) {

        TouchAction touchAction = new TouchAction(driver);

        int numOfSwipes = 0;
        while (numOfSwipes <= 10) {
            touchAction.longPress(PointOption.point(startx, starty))
                    .moveTo(PointOption.point(endx, endy))
                    .release()
                    .perform();
            numOfSwipes++;
        }
    }

    /**
     * This method does a swipe upwards
     *
     * @author Basav
     */
    public void scrollDownByLocator(By locator) {

        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        //Starting y location set to 40% of the height (near mid-screen)
        int starty = (int) (size.height * 0.40);
        //Ending y location set to 20% of the height (near top)
        int endy = (int) (size.height * 0.20);
        //x position set to mid-screen
        int startx = size.width / 2;

        scroll(locator, startx, starty, startx, endy);

    }

    /**
     * This method does a swipe sideways
     *
     * @author Basav
     */
    public void swipeSidewaysByLocator(By locator) {

        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        //Starting y location set to 40% of the height (near mid-screen)
        int startx = (int) (size.width * 0.20);
        //Ending y location set to 20% of the height (near top)
        int endx = (int) (size.width * 0.50);
        //x position set to mid-screen
        int starty = size.height / 2;

        scroll(locator, starty, startx, starty, endx);

    }

    protected void scrollDown() {

        //The viewing size of the device
        Dimension size = driver.manage().window().getSize();

        //Starting y location set to 40% of the height (near mid-screen)
        int starty = (int) (size.height * 0.40);
        //Ending y location set to 20% of the height (near top)
        int endy = (int) (size.height * 0.20);
        //x position set to mid-screen
        int startx = size.width / 2;

        scroll(startx, starty, startx, endy);

    }

    /**
     * Method to perform the left swipe in the screen. Takes no paramters
     */

    protected void swipeHorizontallyToLeft() {
        int screenHeight = driver.manage().window().getSize().getHeight();
        int screenWidth = driver.manage().window().getSize().getWidth();
        swipeAction((int) (screenWidth * .9), (int) (screenHeight * .50), (int) (screenWidth * .02), (int) (screenHeight * .50));
    }

    private void swipeAction(int startX, int startY, int endX, int endY) {
        TouchAction ta = new TouchAction(driver);
        ta.press(PointOption.point(startX, startY)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1500))).moveTo(PointOption.point(endX, endY)).release().perform();
    }

    /**
     * Method to get the attribute value element located by the locator. Takes the By locator as
     * parameter
     *
     * @param locator
     * @author basav
     */
    protected String getAttribute(By locator, String attribute, int... timeout) {
        WebElement element = find(locator, timeout);
        String text;
        if (element != null) {
            text = element.getAttribute(attribute);
            return (text == null) ? "null" : text.toString();
        } else
            return null;
    }

    public void tapLastKeyboardKey_iOS() {
        System.out.println("   tapLastKeyboardKey_iOS()");
        List<WebElement> el = null;
        try {
            el = driver.findElement(By.className("XCUIElementTypeKeyboard")).findElements(By.className("XCUIElementTypeButton"));
        } catch (Exception e) {
            //return false;
        }
        MobileElement NextORGO = (MobileElement) el.get(el.size() - 1);
        Boolean isGoNextEnabled = NextORGO.isDisplayed();
        NextORGO.click();
        //return tapElement((MobileElement) el.get(el.size() - 1),20);
    }

    protected void closeAndLaunchApp() {
        driver.closeApp();
        driver.launchApp();
    }

    /**
     * Type a value into an appropriate element by passing the value to be
     * entered and the element locator
     *
     * @param inputValue
     * @param locator
     */
    protected void typeValue(String inputValue, By locator) {
        find(locator).clear();
        find(locator).sendKeys(inputValue);
    }

    protected void clearValue(By locator) {
        find(locator, 20).clear();
    }

    protected boolean isKeyBoardPresent() {

        if (find(spaceKey) != null && find(nextKey) != null) {

            return true;
        } else {
            return false;
        }

    }

}
