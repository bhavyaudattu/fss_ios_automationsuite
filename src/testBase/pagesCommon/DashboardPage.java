package testBase.pagesCommon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class DashboardPage extends BasePage {
    private By barGraph = MobileBy.xpath("//XCUIElementTypeOther[@name=\"Bar Chart. 1 dataset. \"]");

    //Consolidate My Super Tile
    private By consolidateTitle = MobileBy.AccessibilityId("Consolidate My Super");
    private By consolidateTileDescription = MobileBy.AccessibilityId("Don’t pay more fees! Consolidate your super into a single fund.");
    private By consolidateButton = MobileBy.AccessibilityId("Consolidate Now");

    public DashboardPage(AppiumDriver driver) {
        super(driver);
    }

    public WebElement checkForTheGraph() {
        return find(barGraph);
    }

    public void verifyTheDashboardPageDetails(List<Map<String, String>> list) {

        for (int i = 0; i < list.size(); i++) {
            String errorMessage = list.get(i).get("Field Name") + " is not visible";
            Assert.assertTrue(isElementWithStaticTextEnabled(list.get(i).get("Field Value")), errorMessage);
        }
    }

    //Methods for the consolidate now dynamic tile
    public WebElement checkConsolidateTitle() {

        return find(consolidateTitle);
    }

    public WebElement checkConsolidateDescription() {

        return find(consolidateTileDescription);
    }

    public WebElement checkConsolidateButton() {

        return find(consolidateButton);
    }

    public void clickOnConsolidateNowButton() throws InterruptedException {
//        tapElement(consolidateButton, 30);
        Thread.sleep(5000);
        tapByCoordinates(consolidateButton);
    }


}
