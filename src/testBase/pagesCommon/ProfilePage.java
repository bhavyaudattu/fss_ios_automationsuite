
package testBase.pagesCommon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

/**
 * Collection of all the elements and methods corresponding to Profile screen
 * Login -> Click on Profile Icon to navigate to this screen
 *
 * @author Bhavya Udattu
 */
public class ProfilePage extends BasePage {

    public By shareMembershipDetailsLink = MobileBy.xpath("//XCUIElementTypeStaticText[@value=\"Share Membership Details\"]");
    private By shareMembershipDetailsImage = MobileBy.xpath("//XCUIElementTypeImage[@name=\"share\"]");
    private By membershipDetailsCard = MobileBy.xpath("//XCUIElementTypeImage[@name=\"shadow\"]");

    public By shareMembershipEmailSubject = MobileBy.AccessibilityId("subjectField");

    public ProfilePage(AppiumDriver driver) {
        super(driver);
    }

    public void verifyShareMemberDetails() {
        Assert.assertTrue(isEnabled(shareMembershipDetailsLink), "Share Membership Details static text is not visible");
        Assert.assertTrue(isVisible(shareMembershipDetailsImage), "Share Membership Details icon is not visible");
    }

    public void verifyMembershipDetailsCard(List<Map<String, String>> list) {

        for (int i = 0; i < list.size(); i++) {
            String errorMessage = list.get(i).get("Field Name") + "is not visible";
            Assert.assertTrue(isElementWithStaticTextVisible(list.get(i).get("Field Value")), errorMessage);
        }
        //Is the card present
        Assert.assertTrue(isEnabled(membershipDetailsCard), "Membership Card Details is present");
    }

    public void verifyProfilePageDetails(List<Map<String, String>> list) {

        for (int i = 0; i < list.size(); i++) {
            String errorMessage = list.get(i).get("Field Name") + "is not visible";
            Assert.assertTrue(isElementWithStaticTextVisible(list.get(i).get("Field Value")), errorMessage);
        }
    }

    public void clickOnShareMembershipDetails() throws InterruptedException {
        Thread.sleep(5000);
        tapByCoordinates(shareMembershipDetailsLink);
    }

    public String getShareMemEmailSubject() {
        return getAttribute(shareMembershipEmailSubject, "value");
    }

    public void scrollDownToSecondHalf() {
        scrollDown();
    }
}
