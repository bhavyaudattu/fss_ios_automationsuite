package testBase.pagesCommon;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileBy;

import java.io.IOException;

public class WelcomePage extends BasePage{

	private By welcomeScreenMessage01= MobileBy.AccessibilityId("View Your Balance, Transactions and Fees");
	private By welcomeScreenSubMessage01 = MobileBy.AccessibilityId("Stay up to date with how your super is tracking.");
	private By welcomePageIndicator = MobileBy.xpath("//XCUIElementTypePageIndicator");
	private By skipButton = MobileBy.AccessibilityId("Skip");
	private By nextButton = MobileBy.AccessibilityId("Next");
	private By splashImage = MobileBy.xpath("//XCUIElementTypeImage[@name=\"splash\"]");
	private By welcomePageSwipeIndicator = MobileBy.xpath("//XCUIElementTypeApplication[@name=\"FSS-QA\"]//XCUIElementTypePageIndicator");
	private By welcomeScreenMessage02 = MobileBy.AccessibilityId("View Your Insurance Details");
	private By welcomeScreenSubMessage02 = MobileBy.AccessibilityId("See what you’re covered for and manage your policies.");
	private By welcomeScreenMessage03 = MobileBy.AccessibilityId("Access Your Membership Details");
	private By welcomeScreenSubMessage03 = MobileBy.AccessibilityId("Easily share your details with your new employer when changing jobs.");
	private By doneBtn = MobileBy.AccessibilityId("Done");

	public WelcomePage(AppiumDriver driver) {
		super(driver);
	}

	//Welcome message ONE
	public String getWelcomeScreenMessage01() {

		return getAttribute(welcomeScreenMessage01, "value");
	}

	//Welcome sub message ONE
	public String getWelcomeScreenSubMessage01() {

		return getAttribute(welcomeScreenSubMessage01, "value",25);
	}

	//Welcome message TWO
	public String getWelcomeScreenMessage02() {

		return getAttribute(welcomeScreenMessage02, "value");
	}
	//Welcome sub message TWO
	public String getWelcomeScreenSubMessage02() {

		return getAttribute(welcomeScreenSubMessage02, "value",25);
	}

	//Welcome message THREE
	public String getWelcomeScreenMessage03() {

		return getAttribute(welcomeScreenMessage03, "value");
	}
	//Welcome sub message THREE
	public String getWelcomeScreenSubMessage03() {

		return getAttribute(welcomeScreenSubMessage03, "value",25);
	}

	//Check the welcome page indicator
	public WebElement checkWelcomePageIndicator() {

		return find(welcomePageIndicator);
	}

	//check Skip button existence 
	public WebElement checkSkipBtn() {

		return find(skipButton,30);
	}

	//get Skip button text
	public String getSkipBtnText() {
		return getAttribute(skipButton, "label",20);
	}

	//get next button text
	public String getNextBtnText() {

		return getAttribute(nextButton, "label");
	}

	//check for NEXT button existence 
	public WebElement checkNextBtn() {

		return find(nextButton,20);
	}

	//Tap Skip
	public void tapSkip() {

		tapElement(skipButton);
	}

	//Tap Next
	public void tapNext() {
		tapElement(nextButton);

	}

	public WebElement checkSplashScreen() {
		return find(splashImage);
	}
	public void launchApp() throws IOException, InterruptedException {
		terminateAndLaunchApp();

	}


	public void swipeWelcomeInfo() {
		// TODO Auto-generated method stub
		swipeHorizontallyToLeft();
	}

	public String pageNumber() {
		// TODO Auto-generated method stub
		return getAttribute(welcomePageSwipeIndicator , "value");
	}

	public WebElement checkDoneBtn() {
		// TODO Auto-generated method stub
		return find(doneBtn );
	}

	public String getDoneBtnText() {
		// TODO Auto-generated method stub
		return getAttribute(doneBtn, "label", 25);
	}

	public void tapDoneBtn() {
		tapElement(doneBtn);
		
	}



}
