package testBase.pagesCommon;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;
import testBase.testCommon.TestBase;

import java.io.File;
import java.io.IOException;

public class AutoUtilities extends TestBase{
	
	

    /**
     * This method is invoked from the Assertion failures to take a screenshot automatically when an
     * assert fails
     * 
     * @param timeStamp
     */
     void takeScreenShot(String timeStamp) {
        String filePath = System.getProperty("user.dir") + "/Screenshots/";
       
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        // The below method will save the screen shot in the specified drive with current time stamp
        try {
            FileUtils.copyFile(scrFile, new File(filePath + timeStamp + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String absfileName = filePath + timeStamp + ".png";
        Reporter.log("<a href='" + absfileName + "'> <img src='" + absfileName
                + "' height='100' width='100'/> </a>");
    }

}
