package testBase.pagesCommon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {

    private By loginBtn = MobileBy.AccessibilityId("Login");
    private By inputFieldEmail = MobileBy.xpath("//XCUIElementTypeTextField[@name=\"login.inputField\"]");
    private By inputFieldPwd = MobileBy.xpath("//XCUIElementTypeSecureTextField[@name=\"password.inputField\"]");
    private By spaceKey = MobileBy.AccessibilityId("space");
    private By nextKey = MobileBy.AccessibilityId("Next:");
    private By returnKey = MobileBy.AccessibilityId("Return");
    private By emailMemberNumInlineErrorField = MobileBy.AccessibilityId("login.errorLabel");
    private By pwdInlineErrorField = MobileBy.AccessibilityId("password.errorLabel");
    private By incorrectInfoPopupMessage = MobileBy.AccessibilityId("Your login information doesn’t match our records for you. Please try again.");
    private By incorrectInfoTitle = MobileBy.xpath("//XCUIElementTypeStaticText[@name=\"Incorrect Information\"]");
    private By loginWithFaceIdTitle = MobileBy.AccessibilityId("Log in with Face ID");
    private By loginWithTouchIdTitle = MobileBy.AccessibilityId("Log in with Touch ID");
    private By profileIcon = MobileBy.xpath("//XCUIElementTypeButton[@name=\"Profile\"]");
    private By notNowFaceId = MobileBy.AccessibilityId("Not Now");
    private By forgotPasswordLink = MobileBy.xpath("//XCUIElementTypeApplication[@name=\"FSS-QA\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeButton[2]\n");
    private By termsAndConditionsLink = MobileBy.xpath("//XCUIElementTypeApplication[@name=\"FSS-QA\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[4]");

    //Elements of Reset Password page
    private By resetPasswordTitle = MobileBy.xpath("//XCUIElementTypeStaticText[@name=\"Reset password\"]");
    private By enterDetailsStaticText = MobileBy.xpath("//XCUIElementTypeStaticText[@name=\"STEP 1: Enter details\"]");
    private By memberNumberField = MobileBy.xpath("//XCUIElementTypeOther[@name=\"Member number\"]");

    //Elements of Terms and Conditions
    private By termsAndConditionsContent = MobileBy.xpath("//XCUIElementTypeStaticText[contains(@name, 'FSS Trustee Corporation ABN 11 118 202 672')]");
    private By btnDone = MobileBy.xpath("//XCUIElementTypeButton[@name=\"Done\"]");
    private By btnReload = MobileBy.xpath("//XCUIElementTypeButton[@name=\"ReloadButton\"]");

    public LoginPage(AppiumDriver driver) {
        super(driver);
    }

    public WebElement checkLoginBtn() {

        return find(loginBtn);
    }

    public WebElement checkLoginInputFieldEmail() {

        return find(inputFieldEmail);
    }

    public String getLoginInputFieldEmailDefaultText() {

        //return getAttribute(inputFieldEmail, "value");
        return find(inputFieldEmail).getText();
    }

    public WebElement checkLoginInputFieldPwdEmail() {

        return find(inputFieldPwd);
    }

    public String getLoginInputFieldPwdDefaultText() {

        return getAttribute(inputFieldPwd, "value");
    }

    public boolean isKeyboardShownInLoginPage() {
        return isKeyBoardPresent();
    }

    public void tapEmailMemberId() {
        tapElement(inputFieldEmail);
    }

    public void tapPwd() {
        tapElement(inputFieldPwd);
    }

    public void enterEmailMemberId(String text) {
        tapElement(inputFieldEmail);
        clearValue(inputFieldEmail);
        typeValue(text, inputFieldEmail);
    }

    public void enterPwd(String text) {
        tapElement(inputFieldPwd);
        clearValue(inputFieldPwd);
        typeValue(text, inputFieldPwd);
    }

    public void tapNextInKeyBoard() {
        tapElement(nextKey);
    }

    public void tapReturnInKeyBoard() {
        tapElement(returnKey);
    }

    public boolean keyBoardReturnElementPresent() {
        if (find(returnKey, 25) != null) {
            return true;
        } else {
            return false;
        }

    }

    public String getPwdFieldText() {

        return getAttribute(inputFieldPwd, "value");
    }

    public void tapLoginBtn() {
        tapElement(loginBtn);

    }

    public void clearLoginField() {
        clearValue(inputFieldEmail);

    }

    public void enterEmail(String string) {
        enterEmailMemberId(string);
    }

    public void enterPassword(String string) {
        enterPwd(string);
    }

    public WebElement checkIncorrectInfoPopupTitle() {

        return find(incorrectInfoTitle);
    }

    public WebElement checkIncorrectInfoPopupMessage() {

        return find(incorrectInfoPopupMessage);
    }

    public WebElement checkIfLoginFaceIdTitlePresent() {

        return find(loginWithFaceIdTitle);
    }

    public WebElement checkIfLoginTouchIdTitlePresent() {

        return find(loginWithTouchIdTitle);
    }

    public String getEmailMemberNumberInlineErrorMessage() {

        return getAttribute(emailMemberNumInlineErrorField, "value");
    }

    public String getPasswordInlineErrorMessage() {

        return getAttribute(pwdInlineErrorField, "value");
    }

    public String getEmailMemberNumberFieldValue() {

        return getAttribute(inputFieldEmail, "value");
    }

    public void clickOnNotNow() {
        tapElement(notNowFaceId, 30);
    }

    public void navigateToProfilePage() throws InterruptedException {
        Thread.sleep(5000);
        tapElement(profileIcon, 30);
//        waitFor(profilePage.shareMembershipDetailsLink, 30);
//        System.out.println( "Element is " + find(profilePage.shareMembershipDetailsLink, 30));

    }

    public void clickOnForgotPasswordLink() throws Exception {
        tapElement(forgotPasswordLink, 30);
    }

    public void clickOnTermsAndConditionsLink() {
        tapElement(termsAndConditionsLink, 30);
    }

    public WebElement checkForResetPasswordTitle() {
        return find(resetPasswordTitle);
    }

    public WebElement checkForContentsOfTermsAndConditions() {
        return find(termsAndConditionsContent, 120);
    }

    public WebElement checkForDoneButton() {
        return find(btnDone);
    }

    public WebElement checkForReloadButton() {
        return find(btnReload);
    }

    public WebElement checkForEnterDetailsText() {
        return find(enterDetailsStaticText);
    }

    public WebElement checkForMemberNumberField() {
        return find(memberNumberField, 30);
    }

    public void clickOnDoneButton() {
        tapElement(btnDone);
    }

    public WebElement checkForLoginButton() {
        return find(loginBtn, 120);
    }


}
