package testBase.testCommon;


import org.testng.annotations.BeforeClass;
import testBase.pagesCommon.LoginPage;
import testBase.pagesCommon.ProfilePage;
import testBase.pagesCommon.WelcomePage;
import testBase.pagesCommon.DashboardPage;

public class Pages extends TestBase {

    //page objects declared
    public LoginPage loginPageObj;
    public WelcomePage welcomePageObj;
    public ProfilePage profilePageObj;
    public DashboardPage dashboardPageObj;


    //page objects initialised
    public Pages() {
        welcomePageObj = new WelcomePage(driver);
        loginPageObj = new LoginPage(driver);
        profilePageObj = new ProfilePage(driver);
        dashboardPageObj = new DashboardPage(driver);
    }

    public WelcomePage getWelcompageObject() {
        return welcomePageObj;
    }

    public LoginPage getLoginPageObject() {
        return loginPageObj;
    }

    public ProfilePage getProfilePageObj() {
        return profilePageObj;
    }

    public DashboardPage getDasboardPageObj() {
        return dashboardPageObj;
    }

    @BeforeClass(alwaysRun = true)
    public void beforeEachTest() throws Exception {
        terminateAndLaunchApp();
    }

}
