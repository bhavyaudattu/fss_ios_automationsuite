package testBase.testCommon;

import org.openqa.selenium.By;

import io.appium.java_client.MobileBy;

public interface SettingsApp{
	
	 By settingsTitle = MobileBy.iOSNsPredicateString("type=\"XCUIElementTypeApplication\" AND label =\"Settings\"");
	 By apiTitle = MobileBy.iOSNsPredicateString("type=\"XCUIElementTypeNavigationBar\" AND name =\"API\"");
	 By marketPlaceTitle = MobileBy.iOSNsPredicateString("type=\"XCUIElementTypeNavigationBar\" AND name =\"FSS-QA\"");
	 By settingsIcon = MobileBy.AccessibilityId("Settings");
	 By firstStateSuper = MobileBy.name("FSS-QA");
	 By apiEndPoint = MobileBy.name("Environment");
	 By testServer = MobileBy.name("Test");
	 By prodServer = MobileBy.name("Production");
	 By certificatePinning = MobileBy.name("TLS Certificate Pinning");
//	 By marketPlaceBackButton = MobileBy.iOSNsPredicateString("type=\"XCUIElementTypeButton\" AND label =\"Suncorp App\"");
	 By settingsFSSBackButton = MobileBy.xpath("//XCUIElementTypeButton[@name=\"FSS-QA\"]");
	 By settingsBackButton = MobileBy.xpath("//XCUIElementTypeButton[@name=\"Settings\"]");
	// By settingsBackButton =MobileBy.iOSNsPredicateString("type=\"XCUIElementTypeButton\" AND label =\"Settings\""); 
//	 By devSwitches = MobileBy.name("Dev Switches");
//	 By hideFundsTransferToggle = MobileBy.name("Hide Funds Transfer");
	 
	public void configureEnvironment(String endpoint);
	
}
