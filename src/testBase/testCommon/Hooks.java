package testBase.testCommon;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;




public class Hooks extends TestBase{

   // TestBase newHookdriver = new TestBase();

    
    @BeforeClass
    public void beforeHookfunction() throws InterruptedException, IOException {
       setup();
    }

    @AfterClass
    public void afterHookfunction() throws IOException {
    		teardown();
    }
    	
}
