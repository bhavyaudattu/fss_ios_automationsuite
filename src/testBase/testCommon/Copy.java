package testBase.testCommon;

public interface Copy {
	
	//WELCOME SCREEN
	public String WELCOME_SCREEN_LABEL_ONE = "View Your Balance, Transactions and Fees";
	public String WELCOME_SCREEN_SUB_MESSAGE_ONE = "Stay up to date with how your super is tracking.";
	public String WELCOME_SCREEN_SKIP_BTN = "Skip";
	public String WELCOME_SCREEN_NEXT_BTN = "Next";
	public String WELCOME_SCREEN_LABEL_TWO = "View Your Insurance Details";
	public String WELCOME_SCREEN_SUB_MESSAGE_TWO = "See what you’re covered for and manage your policies.";
	public String WELCOME_SCREEN_LABEL_THREE = "Access Your Membership Details";
	public String WELCOME_SCREEN_SUB_MESSAGE_THREE = "Easily share your details with your new employer when changing jobs.";

	//Share Membership Details Email
	public  String EMAIL_SUBJECT = "Superannuation membership details";

}
