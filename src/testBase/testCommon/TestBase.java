/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~ Author:Basavaraj Biradar  Email : basavaraj.biradar@arq.group
 * Introduction : This is a Java based mobile automation framework built using Appium/Cucumber/Junit.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~
 */

package testBase.testCommon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;

//@SuppressWarnings("rawtypes")

public class TestBase implements SettingsApp {

    // The InitAppium class has the required Appium initialization

    public static AppiumDriver driver = null;

    //public AppiumServiceBuilder service;
    private static AppiumDriverLocalService service = null;
    private String AppiumNodeFilePath = "/usr/local/bin/node";
    private String AppiumJavaScriptServerFile = "/usr/local/lib/node_modules/appium/build/lib/main.js";
    private File app = null;
    private File shellScript = null;
    private String appName = "First_State_Super";
    private String shellScriptName = "kill_appium.sh";

    @SuppressWarnings("static-access")
    //@Parameters({"TEST","NORESET" })
    //@BeforeSuite(alwaysRun = true)
    @BeforeClass
//		public void setup(@Optional("TEST")String endPoint,@Optional("NORESET")String reset) throws IOException, InterruptedException {
    public void setup() throws IOException, InterruptedException {

        /**
         * Start the Appium Server
         * Note if Appium is already running kill the App using shell script
         **/
        shellScript = selectApp(shellScriptName);
        stopAppiumServer(shellScript.getAbsolutePath());
        startAppiumServer();

        // Pick the app and Companion App
        app = selectApp(appName);

        // Provide Capabilities for Appium session
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone X");
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("platformVersion", "12.3");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("udid", "a360c9598530b5ac3416850230e1fba93059b575");
       // capabilities.setCapability("showXcodeLog", true);
//			capabilities.setCapability("fullReset", true);
//			capabilities.setCapability("noReset", false);


        //capabilities.setCapability("fullReset", true);
//			if(reset.equalsIgnoreCase("FULLRESET")) {
//				capabilities.setCapability("fullReset", true);
//				capabilities.setCapability("noReset", false);
//			}else {
//				capabilities.setCapability("fullReset", false);
//				capabilities.setCapability("noReset", true);
//			}

//			capabilities.setCapability("clearSystemFiles", true);
        //capabilities.setCapability("startIWDP", true);

        TestBase.driver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        //configureEnvironment("TEST");
        //terminateAndLaunchApp();

    }


    public void terminateAndLaunchApp() throws IOException, InterruptedException {

        setup();
        //try passing bundle ID dynamically-TBC-------------
//		driver.terminateApp("au.com.fss.FirstStateSuper-QA");
//		driver.activateApp("au.com.fss.FirstStateSuper-QA");

    }

    /**
     * This method is used to start the Appium Server automatically with help of
     * AppiumDriverLocalService and AppiumServiceBuilder classes which help in
     * creating and starting a service by executing nodejs executable and Appium
     * JavaScript server file main.js
     *
     * @return void
     * @author Basav
     */
    private void startAppiumServer() throws IOException, InterruptedException {

        System.out.println("Starting Appium Server ......");

        // Add .withArgument(GeneralServerFlag.LOG_LEVEL, "error")) to run
        // service in Error Log (By default it will run in Debug mode)
        service = AppiumDriverLocalService
                .buildService(new AppiumServiceBuilder().usingDriverExecutable(new File(AppiumNodeFilePath))
                        .withAppiumJS(new File(AppiumJavaScriptServerFile)));
        // Start Appium service
        service.start();

        System.out.println("Appium Server Started !!");
    }

    /**
     * This method is used to stop the Appium Server automatically,by stopping
     * the service that was started by startAppiumServer method
     *
     * @return void
     * @author Basav
     */
    private void stopAppiumServer(String scriptFileName) throws IOException {
        try {

            killAppium(scriptFileName);
            System.out.println("Appium Server is now Stopped!!");
        } catch (NullPointerException e) {
            System.out.println("Appium Server is already Stopped !!");
        }
    }


    private void killAppium(String scriptFileName) {
        System.out.println("Inside KILL APPIUM!!!!!");
        service.stop();
        ProcessBuilder pb = new ProcessBuilder(scriptFileName);
        try {
            Process p;
            p = pb.start();
            System.out.println("Inside KILLED APPIUM!!!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Select the app for automation testing
    private File selectApp(String appName) {
        File appDir = new File("resources");
        File app = null;
        File[] listOfFiles = appDir.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].getName().contains(appName)) {
                app = listOfFiles[i];
                break;
            }
        }
        return app;
    }

    @AfterClass
    public void teardown() throws IOException {
        //driver.closeApp();
        driver.quit();
        //  stopAppiumServer();
    }

    @Override
    public void configureEnvironment(String endpoint) {

        driver.runAppInBackground(Duration.ofSeconds(-1));
        driver.findElement(settingsIcon).click();
        waitFor(settingsTitle);
        scrollToWebElement(firstStateSuper);
        driver.findElement(firstStateSuper).click();
        driver.findElement(certificatePinning).click();

        if (endpoint.equalsIgnoreCase("test")) {
            driver.findElement(apiEndPoint).click();
            driver.findElement(testServer).click();
            driver.findElement(settingsFSSBackButton).click();
            driver.findElement(settingsBackButton).click();
        } else if (endpoint.equalsIgnoreCase("prod")) {
            driver.findElement(apiEndPoint).click();
            driver.findElement(prodServer).click();
            driver.findElement(settingsBackButton).click();
        }
        waitFor(settingsTitle);
        driver.runAppInBackground(Duration.ofSeconds(-1));
        //driver.findElement(settingsIcon).click();
    }

    public void scrollToWebElement(By locator) {

        RemoteWebElement element = (RemoteWebElement) driver.findElement(locator);
        String elementID = element.getId();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("element", elementID);
        scrollObject.put("toVisible", "Any value");
        driver.executeScript("mobile:scroll", scrollObject);
    }

    public void waitFor(By locator, int... args) {

        int timeout = (args.length > 0 ? args[0] : 5);
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

    }
}
