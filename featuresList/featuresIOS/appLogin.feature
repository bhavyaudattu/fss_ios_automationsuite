Feature: Verify the login page

	Scenario: Enter password
		When I launch the FSS Application
		And I navigate to the Login Screen
		Given I am on the login screen
		When I select the password field
		Then show cursor in the field
		And open keyboard with CTA of 'return' which will dismiss the keyboard
		And hide ghost text when the first character is entered
		And mask each character entered in the password field 
		
	Scenario: Successful Login
		When I launch the FSS Application
		And I navigate to the Login Screen
		When I select the email/member number field
		Then I enter "<email-member-number>" in the email/member Id field
		|65159039|
		When I select the password field
		Then I enter "<password>" in the password field
		|Pillar/1|
		Then I select the login button