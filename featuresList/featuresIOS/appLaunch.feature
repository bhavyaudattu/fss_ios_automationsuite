Feature: Welcome screen and Login scenarios

  # The second example has three steps 
	Scenario: Verify the welcome screen of the APP
		When I launch the FSS Application
		#Then show splash screen as normal
		And then show the first welcome screen
		When the splash screens finish showing
		And I am not currently logged in
		Then show the login screen with ghost text
		When I select the email/member number field
	    And open keyboard with CTA of 'next' which will shift the focus to the next field password
		And hide ghost text when the first character is entered
