Feature: Verify the Terms and Conditions on the Login page

  @Test{'MOB-243'} @Low @IOS
  Scenario: Verify the Terms and Conditions External Link
    When I launch the FSS Application
    And I navigate to the Login Screen
    Then I click on the Terms and Conditions Link
    And I verify contents of the Terms and Conditions Web View
    Then I click on the Done button
    And I verify I am on Login Page