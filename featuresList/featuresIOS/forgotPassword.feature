Feature: Verify the Forgot Password External link

  @Test{'MOB-299'} @Low @IOS
  Scenario: Verify the Forgot Password External link
    When I launch the FSS Application
    And I navigate to the Login Screen
    Then I click on the Forgot Password Link
    And I verify contents of the Reset Password Web View