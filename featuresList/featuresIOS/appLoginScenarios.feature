Feature: Verify the flows for user login

  @Test{'MOB-170'} @High @IOS
  Scenario: Login with incorrect credentials.
    When I launch the FSS Application
    And I navigate to the Login Screen
    When I select the email/member number field
    Then I enter "<email-member-number>" in the email/member Id field
      | 65159039 |
    When I select the password field
    Then I enter "<password>" in the password field
      | Hello123 |
    Then I select the login button
    Then Assert for Incorrect Information popup

  @Test{'MOB-1382','MOB-1384','MOB-169'} @High @IOS
  Scenario: Verify mask email details on log in screen
    When I launch the FSS Application
    And I navigate to the Login Screen
    When I select the email/member number field
    Then I enter "<email-member-number>" in the email/member Id field
      | 65159039 |
    When I select the password field
    Then I enter "<password>" in the password field
      | Pillar/1 |
    Then I select the login button
    Then I verify and close the face Id or touch Id popup
    Then I launch the FSS Application
    And I navigate to the Login Screen
    When I select the email/member number field
    And I verify the email/member number field value
      | Member ID ending with: 039 |
    When I enter "<email-member-number>" in the email/member Id field
      | a |
    And I select the email/member number field
    Then I select the login button
#    And I verify the email/member number field value
#      | a |

  @Test{'MOB-157','MOB-156'} @High @IOS
  Scenario: Hide inline error for missing fields
    When I launch the FSS Application
    And I navigate to the Login Screen
    When I select the email/member number field
    Then I enter "<email-member-number>" in the email/member Id field
      |  |
    When I select the password field
    Then I enter "<password>" in the password field
      |  |
    Then I select the login button
    Then I see inline errors for email/member and password fields
      | Please enter your email/member number. |
      | Please enter your password.            |
    When I enter "<email-member-number>" in the email/member Id field
      | member id |
    And I enter "<password>" in the password field
      | password |
    Then I see NO inline errors for email/member and password fields





