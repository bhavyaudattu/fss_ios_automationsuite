Feature: Verify the user Profile

  @Test{'MOB-238','MOB-239'} @High @IOS
  Scenario: Verify the Consolidate Now web view
    When I launch the FSS Application
    And I navigate to the Login Screen
    Then I enter "<email-member-number>" in the email/member Id field
      | 65159039 |
    Then I enter "<password>" in the password field
      | Pillar/1 |
    Then I select the login button
    Then I verify and close the face Id or touch Id popup
    And I verify I am on the Dashboard Page
    And I verify the following details on the Dashboard
      | Field Name           | Field Value        |
      | Account Type         | Employer Sponsored |
      | Member Account       | FSSU_HK9726        |
      | Total Balance Amount | $5,722.31          |
    Then I scroll down to view the other contents
    And I verify the details of Consolidate My Super card
    And I click on the Consolidate Now button
    And I verify consolidate web view page details

