Feature: Verify the user Profile

  @Test{'MOB-167','MOB-465','MOB-470'} @Critical @IOS
  Scenario: Verify the card details from the User Profile Screen
    When I launch the FSS Application
    And I navigate to the Login Screen
    Then I enter "<email-member-number>" in the email/member Id field
      | 65159039 |
    Then I enter "<password>" in the password field
      | Pillar/1 |
    Then I select the login button
    Then I verify and close the face Id or touch Id popup
    And I navigate to the Profile Screen
    And I verify the card details on the Profile Screen
      | Field Name           | Field Value        |
      | Member Name          | Ilya Levchenko     |
      | Member Number Label  | Member Number      |
      | Member Number Value  | 65159039           |
      | Fund Name Label      | Fund Name          |
      | Fund Name Value      | First State Super  |
      | Fund ABN Label       | Fund ABN           |
      | Fund ABN Label Value | 53 226 460 365     |
      | Fund USI Label       | Fund USI           |
      | Fund USI Label Value | 53 226 460 365 001 |
    And I verify for the following fields on the Profile Section
      | Field Name                | Field Value                 |
      | Date of Birth Label       | Date of Birth               |
      | Date of Birth Value       | 08/02/1980                  |
      | Tax File Number Label     | Tax File Number             |
      | Tax File Number Value     | Provided                    |
      | Residential Address Label | Residential Address         |
      | Residential Address Value | 67 Floy St, Sydney NSW 2000 |
    Then I scroll down to view the other contents
    And I verify for the following fields on the Profile Section
      | Field Name          | Field Value                   |
      | Email Address Label | Email Address                 |
      | Email Address Value | ilya.levchenko@outware.com.au |
      | Mobile Number Label | Mobile Number                 |
      | Mobile Number Value | 0402450200                    |


  @Test{'MOB-138'} @Critical @IOS
  Scenario: Verify the Share membership content details
    When I launch the FSS Application
    And I navigate to the Login Screen
    Then I enter "<email-member-number>" in the email/member Id field
      | 65159039 |
    Then I enter "<password>" in the password field
      | Pillar/1 |
    Then I select the login button
    Then I verify and close the face Id or touch Id popup
    And I navigate to the Profile Screen
    And I verify the card details on the Profile Screen
      | Field Name          | Field Value    |
      | Member Name         | Ilya Levchenko |
      | Member Number Label | Member Number  |
    Then I click on Share Membership Details
    And I verify the contents of the Email Subject
    And I verify the contents of the Email Body
      | Field Name               | Field Value                                                                                                                                                          |
      | Employee details heading | Employee details                                                                                                                                                     |
      | Employee Name            | - Employee name: Ilya Levchenko                                                                                                                                      |
      | Member Number            | - Member number: 65159039                                                                                                                                            |
      | Member Account Number    | - Member Account Number: FSSU_HK9726                                                                                                                                 |
      | Initial Email Content    | Your employee Ilya Levchenko has requested that their super contributions be paid into their First State Super account. Details of their request can be found below. |